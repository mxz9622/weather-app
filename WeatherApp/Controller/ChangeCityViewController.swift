//
//  ChangeCityViewController.swift
//  WeatherApp
//
//  Created by Marko Živko on 28/05/2019.
//  Copyright © 2019 Marko Živko. All rights reserved.
//

import UIKit
import JGProgressHUD

//MARK: part6 - writing the protocol
//after updateUI is done
protocol ChangeCityDelegate {
    
    //if you want to be the delegate you need to implements this method
    func userEnteredACityName(city: String)
}

class ChangeCityViewController: UIViewController {

    //part6
    var delegate: ChangeCityDelegate?
    //and now add protocol to WeatherViewController
    
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var cityNameTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Trazi grad"
        
        self.navigationController?.navigationBar.backItem?.title = "Back"
        
        navigationItem.largeTitleDisplayMode = .never
        
        //rounded button settings
        searchButton.layer.cornerRadius = 8
        searchButton.layer.borderWidth = 1
        searchButton.layer.borderColor = UIColor.white.cgColor
        
      
        
    }
    

    //MARK: part6
    //after vc.delegate is set in searchCity objc method
    @IBAction func searchCityButton(_ sender: UIButton) {
        
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Searching for city"
        hud.show(in: self.view)
        hud.dismiss(afterDelay: 1.0)
        
        //get the city name that user entered in the text field
        let cityName = cityNameTextField.text!
        
        //if delegate is set we can call the method
        //used with ? - optional chaining 
        delegate?.userEnteredACityName(city: cityName)
        
        //dismiss view controller
        //TODO: transition not working
        self.dismiss(animated: true, completion: nil)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
