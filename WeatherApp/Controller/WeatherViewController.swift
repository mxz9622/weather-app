//
//  WeatherViewController.swift
//  WeatherApp
//
//  Created by Marko Živko on 28/05/2019.
//  Copyright © 2019 Marko Živko. All rights reserved.
//

import UIKit
import CoreLocation
//import Alamofire and SwiftyJSON (later) - command + B to build if error thrown
import Alamofire
import SwiftyJSON

//changeCityProtocol is part6
class WeatherViewController: UIViewController, CLLocationManagerDelegate, ChangeCityDelegate {

    //MARK: part1
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var cityNameLabel: UILabel!
    
    //constants
    let weather_url = "https://api.openweathermap.org/data/2.5/weather"
    let app_id = "72cedcb476435ed1f3a7172a7e52d7e6"
    
    //instance var
    let locationManager = CLLocationManager()
    //MARK: part4
    let weatherDataModel = WeatherDataModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Vrijeme danas"
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Novi grad", style: .plain, target: self, action: #selector(searchCity))
        
       // navigationItem.backBarButtonItem?.title = "Back"

        
        navigationController?.navigationBar.prefersLargeTitles = true
        
        //MARK: part2 - location manager
        
        //we are delegating all the functionalities from protocol CLLocationManagerDelegate
        locationManager.delegate = self
        
        //specify the best accuracy for your usage
        //better the accuracy heavier the usage of memory and battery
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        
        /*
         ask user for permsission to get their location
         we also need to add info to plist in order to see popup
         we can open Info.plis also as xml
         also we need to add this code to plist in order to work with http
        */
        locationManager.requestWhenInUseAuthorization()
        
        //start looking GPS location
        //it works in the backgroud - asynchronous method
        locationManager.startUpdatingLocation()
        
        
        // Do any additional setup after loading the view.
    }
    
    
    //MARK: part1
    @objc func searchCity(){
    
        if let vc = storyboard?.instantiateViewController(withIdentifier: "citySearch") as? ChangeCityViewController{
            
            //MARK: part6
            vc.delegate = self
            
            navigationController?.pushViewController(vc, animated: true)
            
        }
    
    }
    
    //MARK: part2 - location manager
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        //last value in the array [CLLocation] is the most accurate
        let location = locations[locations.count - 1]
        
        //we need to make sure if the location is valid
        if location.horizontalAccuracy > 0{
            
            //as soon as we find the valid location we stop updating
            // since this a very heavy process we are killing the battery
            locationManager.stopUpdatingLocation()
            
            //can add this later with JSON parsing to stop printing data multiple times
            locationManager.delegate = nil
            
            print("longitude = \(location.coordinate.longitude), latitude = \(location.coordinate.latitude)")
            
            //need to be converted to string (do it later)
            let latitude = String(location.coordinate.latitude)
            let longitude = String(location.coordinate.longitude)
            
            //create dictionary to contain parameters, look at API docs to name the KEYS
            let params: [String : String] = ["lat": latitude, "lon": longitude, "appid": app_id]
            
            //MARK: part3 - HTTP calls
            //making the HTTP calls - Alamofire (simplifying http request)
            getWeatherData(url: weather_url, parameters: params)
            
            
        }
        
        
    }
    
    //MARK: part3
    //networking with Alamofire helper method
    func getWeatherData(url: String, parameters: [String : String]){
        
        //handling response from openweather api servers - asynchronous process
        /*
         requesting the data
         method - get (what we want to do with data), we are retreiving the data
         parameters - comming from dictionary params
        */
        Alamofire.request(url, method: .get, parameters: parameters).responseJSON{
            response in
            if response.result.isSuccess{
                
                print("success! We got the weather data!")
                //we can use ! (force unwrapping) since we already checked that response is successfull
                //we alse need to convert it to JSON data type (later)
                let weatherJSON: JSON = JSON(response.result.value!)
                
                print(weatherJSON)
                //copy and paste the json from console to JSON Editor online
                //check objects and structure in more human readable way
                
                //we need self since this is a CLOSURE (func inside a func) keyword IN
                self.updateWeatherData(json: weatherJSON)
                //now you can delete print weather json
                
            }else{
                
                //can be done also without "describing"
                print("Error: \(String(describing: response.result.error))")
                self.cityNameLabel.text = "Connection issues"
            }
        }
        
    }
    
    //MARK: part4 - json parsing
    //create WeatherDataModel and populate with variables
    func updateWeatherData(json: JSON){
        
        //navigating to the temp value inside the json
        //use without IF for the first time - add if let after whole method is done
        if let tempResult = json["main"]["temp"].double{
        //create WeatherDataModel class in order to manipulater with data
        
        //after object is initialized
        
        //substract 273.15 since this is in Kelvins
        //change to Int because of temperature value and force unwrap
        weatherDataModel.temperature = Int(tempResult - 273.15)
        
        //we need to parse it to string
        weatherDataModel.city = json["name"].stringValue
        
        //we can have more weather conditions for the same city - big city
        //we need to parse it to int
        weatherDataModel.condition = json["weather"][0]["id"].intValue
        
        //now go to WeatherDataModel and create func for checking id and presenting valid image
        //now we can add final value - weatherIconName
        weatherDataModel.weatherIconName = weatherDataModel.updateWeatherIcon(condition: weatherDataModel.condition)
            
        //update UI - part5
        updateUIWithWeatherData()
            
        }else{
            cityNameLabel.text = "City not found..."
        }
        
        //now we can try to mess up the APP ID and there will be a nil value crash since
        //we are forcely unwrapping our optional value - we need to use if let instead
        //add zeros to the end of app_id to demonstrade the error
        
    }
    
    //MARK: part5 - update UI
    
    func updateUIWithWeatherData(){
        
        cityNameLabel.text = weatherDataModel.city
        //ctrl + comm + space to pull degree simbol
        temperatureLabel.text = String(weatherDataModel.temperature) + "℃"
        weatherImage.image = UIImage(named: weatherDataModel.weatherIconName)
        
        //for extras we can add toggle switch which will toggle between celsius and fahrenheit degrees 
        
    }
    
    //MARK: part6 - search for new city
    func userEnteredACityName(city: String) {
        print(city)
        
        //after we see that printing works we can continue
        
        //how can we now the key name for param city - it is q
        //visit API documentation for more explanation
        //https://openweathermap.org/current
        let params: [String: String] = ["q": city, "appid": app_id]
        
        getWeatherData(url: weather_url, parameters: params)
        
    }
    
    //first thing is to go to ChangeCityViewController and to create our own protocol 
    
    //handle errors - part2
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
        
        cityNameLabel.text = "Location Unavailable ..."
    }
   

}
