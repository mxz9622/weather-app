//
//  WeatherDataModel .swift
//  WeatherApp
//
//  Created by Marko Živko on 03/06/2019.
//  Copyright © 2019 Marko Živko. All rights reserved.
//

import Foundation

class WeatherDataModel{
    
    /*we will create a model for our weather data
    ** first we will create 4 variables
    **condition - description needs to be int since it is retreived by ID*/
    
    var temperature: Int = 0
    var condition: Int = 0
    var city: String = ""
    var weatherIconName: String = ""
    
    //after creating variables create object weatherDataModel below locationManager inside WeatherController
    
    // done after condition is created inside updateWeatherData()
    //this method is used to present weather icon with ID of the weather condition
    func updateWeatherIcon(condition: Int) -> String{
        
        switch condition {
        case 0...300:
            return "tstorm1"
        case 301...500:
            return "light_rain"
        case 501...600:
            return "shower3"
        case 601...700:
            return "snow4"
        case 701...771:
            return "fog"
        case 772...779:
            return "tstorm3"
        case 800:
            return "sunny"
        case 801...804:
            return "cloudy2"
        case 900...903, 905...1000:
            return "tstorm3"
        case 903:
            return "snow5"
        case 904:
            return "sunny"
        default:
            return "dunno"
        }
        
    }
    
}
